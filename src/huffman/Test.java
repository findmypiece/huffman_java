package huffman;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.ObjectOutputStream;

public class Test {
	
	public static void main(String[] args) throws Exception{
		
//		HuffmanCode.encoding(new File("C:\\Users\\user\\Desktop\\테스트\\Huffman.java"));
		HuffmanCode.decoding(new File("C:\\Users\\user\\Desktop\\테스트\\Huffman.bin"));
		
		
//		System.out.println(Long.parseLong("11111111111111111111111111111111", 2));
		
	
	
//		File file = new File("C:\\Users\\user\\Desktop\\Test.bin");
//		System.out.println(file.getName().substring(file.getName().lastIndexOf(".")));
		
		
//		int [] intArr = {255,255,255,255,255};
//		
//		ObjectOutputStream dos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(new File("C:\\Users\\user\\Desktop\\Test.bin"))));
//		dos.writeObject(intArr);
//		
//		dos.close();
		
		
//		BufferedWriter bw = new BufferedWriter(new FileWriter(new File("C:\\Users\\user\\Desktop\\Test2.bin")));
//		bw.write("255 255 255 255 255");
//		
//		bw.close();
		
		/**
		 * <압축>
		 * 1. 압축시에는 10진수를 2진 문자열을 생성한다.
		 * 2. 그렇게 생성된 문자열을 8자리씩 잘라서 10진수로 변환한다.
		 *    이렇게 생성될 수 있는 최대 10진수는 255이다.(8비트로 표현할 수 있는 최대 2진수)
		 * 3. 2번에서 생성한 10진수들을 list에 담고 해당 리스트를 트리객체에 포함시킨다.
		 * 4. 이렇게 생성된 트리객체를 ObjectOutputStream으로 파일로 생성한다.
		 * 
		 * <압축해제>
		 * 1. ObjectIntputStream 으로 파일을 읽어들인다.
		 * 2. 10진수 list를 돌면서 2진문자열을 복원한다.
		 * 3. 변환한 문자열로 트리를 순회하며 원본문자열을 복원한다.
		 * 4. 변환된 문자열로 파일을 생성한다.
		 *    
		 * */
		
	}

}
